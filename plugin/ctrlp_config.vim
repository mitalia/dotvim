" remap to ctrl-k and use mixed mode (vaguely similar to Qt Creator)
let g:ctrlp_map = '<c-k>'
let g:ctrlp_cmd = 'CtrlPMixed'
" always open the file in the current window, don't jump around
let g:ctrlp_switch_buffer = '0'
" ctrl-p just for the buffers
nnoremap <C-P> :CtrlPBuffer<CR>
