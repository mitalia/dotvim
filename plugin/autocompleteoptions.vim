set ofu=syntaxcomplete#Complete
set completeopt+=longest
set wildmode=longest,list,full
set wildmenu
set completeopt=menuone,menu,longest,preview

if has('unix')
    let $PATH=$PATH.":"."$HOME/bin"
endif
