" Purge the given BBCode tag from the selected range
function! BBKill(bbTag) range
    execute (a:firstline) . "," . a:lastline . 's~\[/*' . a:bbTag . '[^\]]*\]~~gi'
endfunction

" Fix indentation in a BBCode forum post with C-like syntax
function! CPostFix()
    " open a comment at the begin and at the end of the post
    :1s~^~/*CPOSTFIX-TEMP~
    :$s~$~CPOSTFIX-TEMP*/~
    " close the comment just after the start of a CODE block
    :%s~\[code\]~[code]CPOSTFIX-TEMP*/~gi
    " re-open the comment just before the end of the CODE block
    :%s~\[/code\]~/*CPOSTFIX-TEMP[/code]~gi
    " fix indentation via astyle
    :%!astyle
    " kill all the temporary fixups
    :%s~\(/\*\)\?CPOSTFIX-TEMP\(\*/\)\?~~g
endfunction
