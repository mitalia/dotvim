let g:jedi#rename_command = "<leader><leader>r"
let g:jedi#usages_command = "<leader><leader>u"
let g:jedi#documentation_command = "<leader><F1>"
let g:jedi#goto_command = "<F2>"
let g:jedi#auto_close_doc = 0
