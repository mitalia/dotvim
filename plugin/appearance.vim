if &term == 'xterm' || &term =~? '^screen'
    set t_Co=256
endif
if $COLORTERM=="konsole"
    let g:CSApprox_konsole=1
endif
if has("gui_running") || &t_Co>=256
    colorscheme distinguished
else
    let g:CSApprox_loaded=0
    colorscheme default
endif

if has("gui_running") && has('win32')
    set guifont=DejaVu\ Sans\ Mono:h11
endif

