syntax on
set encoding=utf-8
set nu
set expandtab
set shiftwidth=4
set softtabstop=4
set autoindent
set autochdir
set hlsearch
set incsearch
set ignorecase
set smartcase
set backspace=2
set noswapfile
"set synmaxcol=255
set foldlevelstart=99
filetype plugin indent on
let g:xml_syntax_folding=1
au FileType xml setlocal foldmethod=syntax
if has('python3')
    let g:gundo_prefer_python3 = 1
endif
