" easily perform operations on the system clipboard
nnoremap <leader>p "+p
nnoremap <leader>y "+y
nnoremap <leader>d "+d
nnoremap <leader>P "+P
nnoremap <leader>Y "+Y
nnoremap <leader>D "+D
nnoremap <leader>v :let @@=@+<CR>
nnoremap <leader>c :let @+=@@<CR>

" quick make
nnoremap <leader>m :make<CR>

" in normal mode use screen-line movements
nnoremap <home> g0
nnoremap <end> g$
nnoremap <up> gk
nnoremap <down> gj
nnoremap <home> g0
nnoremap <end> g$

" panic button
nnoremap <leader><space> :noh<cr>:pclose<cr>:cclose<cr>:set nolist<cr>

" show/hide tabs/newlines
nmap <leader>r :set invlist<cr>

" folds
nmap + zo
nmap - zc
nmap <leader>+ zR
nmap <leader>- zM

" shortcuts for header-source switch
nmap <silent> <Leader>of :FSHere<cr>
nmap <silent> <Leader>ol :FSRight<cr>
nmap <silent> <Leader>oL :FSSplitRight<cr>
nmap <silent> <Leader>oh :FSLeft<cr>
nmap <silent> <Leader>oH :FSSplitLeft<cr>
nmap <silent> <Leader>ok :FSAbove<cr>
nmap <silent> <Leader>oK :FSSplitAbove<cr>
nmap <silent> <Leader>oj :FSBelow<cr>
nmap <silent> <Leader>oJ :FSSplitBelow<cr>

" some concessions to Qt Creator
nmap <F4> :FSHere<cr>
nnoremap <A-right> <C-I>
nnoremap <A-left> <C-O>

" TAB in normal/visual mode does autoindent
nmap <TAB> ==
vmap <TAB> =

" NERDTree shortcuts
nmap <leader>t :NERDTreeToggle<cr>
nmap <leader>f :NERDTreeFind<cr>

" Gundo shortcut
nmap <leader>u :GundoToggle<cr>

" Firefox-like tab move
nmap <C-S-PageDown> :tabmove +1<cr>
nmap <C-S-PageUp>   :tabmove -1<cr>

" Shortcut to have the current line ready-made on the command line
" (useful if the command will be reused, but I want to keep the starting position)
nmap <leader>: :<C-R>=line(".")<CR>,

" Bind it to ,
nmap , <Plug>VisualMarksGetVisualMark

" Enable mouse
set mouse=a

" If we have terminal mode, add terminal escapes
if exists(':tnoremap')
    " Simpler way to get out
    tnoremap <Esc><Esc> <C-\><C-n>
    " Ease window switching
    tnoremap <C-w> <C-\><C-n><C-w>

    " Quickly open a terminal
    nmap <leader>t :e term://fish<cr>
endif
