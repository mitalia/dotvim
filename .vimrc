set nocompatible
if filereadable($HOME . "/.vim/.vimrc_local")
    source ~/.vim/.vimrc_local
endif
if &shell =~# 'fish$'
    set shell=sh
endif
filetype off
if has('win32')
    set rtp+=%HOME%/vimfiles/bundle/Vundle.vim/
    call vundle#begin('%USERPROFILE%/vimfiles/bundle/')
else
    set rtp+=~/.vim/bundle/Vundle.vim
    call vundle#begin()
endif

Plugin 'gmarik/Vundle.vim'
Plugin 'vim-scripts/argtextobj.vim'
Plugin 'godlygeek/csapprox'
Plugin 'derekwyatt/vim-fswitch'
Plugin 'sjl/gundo.vim'
Plugin 'michaeljsmith/vim-indent-object'
Plugin 'Lokaltog/vim-distinguished'
Plugin 'itchyny/lightline.vim'
Plugin 'vim-scripts/TeX-9'
Plugin 'matze/vim-ini-fold'
Plugin 'sgeb/vim-diff-fold'
Plugin 'chrisbra/NrrwRgn'
Plugin 'dag/vim-fish'
Plugin 'ctrlpvim/ctrlp.vim'
Plugin 'zah/nim.vim'
Plugin 'elzr/vim-json'
Plugin 'anntzer/vim-cython'

if has('nvim')
    let g:ghost_darwin_app=""
    Plugin 'raghur/vim-ghost'
endif

if exists('g:vim_full')
    Plugin 'davidhalter/jedi-vim'
endif

call vundle#end()
