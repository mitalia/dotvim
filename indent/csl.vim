" Vim indent file
" Language:	CSL
" Maintainer:	Matteo Italia <mitalia@comelz.com>
" Last Change:	2015 Ago 13

" Only load this indent file when no other was loaded.
if exists("b:did_indent")
   finish
endif
let b:did_indent = 1

" C indenting is not too bad.
setlocal cindent
setlocal cinoptions=j1,J1,#1,:0,L0,=0,i0
setlocal cinkeys-=0#

let b:undo_indent = "setl cin<"
