" Vim syntax file
" Language:	CSL
" Maintainer:	Matteo Italia <mitalia@comelz.com>
" Last Change:	2015 Aug 13

if !exists("main_syntax")
  if version < 600
    syntax clear
  elseif exists("b:current_syntax")
    finish
  endif
  let main_syntax = 'csl'
elseif exists("b:current_syntax") && b:current_syntax == "csl"
  finish
endif

let s:cpo_save = &cpo
set cpo&vim

syn keyword cslCommentTodo      TODO FIXME XXX TBD contained
syn match   cslLineComment      "#.*" contains=@Spell,cslCommentTodo
syn match   cslCommentSkip      "^[ \t]*\*\($\|[ \t]\+\)"
syn match   cslSpecial	       "\\\d\d\d\|\\."
syn region  cslStringD	       start=+"+  skip=+\\\\\|\\"+  end=+"\|$+	contains=cslSpecial,@htmlPreproc

syn match   cslSpecialCharacter "'\\.'"
syn match   cslNumber	       "-\=\<\d\+L\=\>\|0[xX][0-9a-fA-F]\+\>"

syn keyword cslConditional	if else switch
syn keyword cslRepeat		while for do in
syn keyword cslBranch		break continue
syn keyword cslOperator		typeof
syn keyword cslType		array str int float dict
syn keyword cslStatement	return using
syn keyword cslNull		null
syn keyword cslBools		true false
syn keyword cslIdentifier	var let
syn keyword cslMessage		msgbox
syn keyword cslGlobal		globals
syn keyword cslReserved		le ge gt lt eq ne def include

syn match   cslGlobalVar        "$[a-zA-Z0-9_]\+\>"

if exists("csl_fold")
    syn match	cslFunction	"\<def\>"
    syn region	cslFunctionFold	start="\<def\>.*[^};]$" end="^\z1}.*$" transparent fold keepend

    syn sync match cslSync	grouphere cslFunctionFold "\<def\>"
    syn sync match cslSync	grouphere NONE "^}"

    setlocal foldmethod=syntax
    setlocal foldtext=getline(v:foldstart)
else
    syn keyword cslFunction	def
    syn match	cslBraces	   "[{}\[\]]"
    syn match	cslParens	   "[()]"
endif

syn sync fromstart
syn sync maxlines=100

if main_syntax == "csl"
  syn sync ccomment cslComment
endif

" Define the default highlighting.
" For version 5.7 and earlier: only when not done already
" For version 5.8 and later: only when an item doesn't have highlighting yet
if version >= 508 || !exists("did_csl_syn_inits")
  if version < 508
    let did_csl_syn_inits = 1
    command -nargs=+ HiLink hi link <args>
  else
    command -nargs=+ HiLink hi def link <args>
  endif
  HiLink cslLineComment		Comment
  HiLink cslCommentTodo		Todo
  HiLink cslSpecial		Special
  HiLink cslStringD		String
  HiLink cslCharacter		Character
  HiLink cslSpecialCharacter	cslSpecial
  HiLink cslNumber		cslValue
  HiLink cslConditional		Conditional
  HiLink cslRepeat		Repeat
  HiLink cslBranch		Conditional
  HiLink cslOperator		Operator
  HiLink cslType			Type
  HiLink cslStatement		Statement
  HiLink cslFunction		Function
  HiLink cslBraces		Function
  HiLink cslError		Error
  HiLink javaScrParenError		cslError
  HiLink cslNull			Keyword
  HiLink cslBools			Keyword

  HiLink cslIdentifier		Identifier
  HiLink cslGlobalVar		Identifier
  HiLink cslMessage		Keyword
  HiLink cslGlobal		Keyword
  HiLink cslMember		Keyword
  HiLink cslReserved		Keyword

  delcommand HiLink
endif

let b:current_syntax = "csl"
if main_syntax == 'csl'
  unlet main_syntax
endif
let &cpo = s:cpo_save
unlet s:cpo_save

" vim: ts=8
